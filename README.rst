
README
======

This folder contains the code used for `10.1016/j.cell.2018.10.004 <https://doi.org/10.1016/j.cell.2018.10.004>`_.
If you use this code in a publication, please cite:

.. admonition:: Citation
   :class: note

   Oriol Pich, Ferran Muiños, Radhakrishnan Sabarinathan, Iker Reyes-Salazar, Abel Gonzalez-Perez,
   Nuria Lopez-Bigas, Somatic and germline mutation periodicity follow the orientation of the DNA minor
   groove around nucleosomes, Cell (2018) doi: `10.1016/j.cell.2018.10.004 <https://doi.org/10.1016/j.cell.2018.10.004>`_

The exact version for reproducing the results is under the Tag Paper. Further improvements in the code can be found in the master branch.

A brief description of the structure of this repo:

- `accessibility <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/nucleosome-periodicity/raw/master/accessibility/accessibility.ipynb>`_: accessibility data analysis
- `ancestral <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/nucleosome-periodicity/raw/master/ancestral/ancestral.ipynb>`_: ancestral states analysis
- `damage <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/nucleosome-periodicity/raw/master/damage/damage.ipynb>`_: damage data (UV and NMP) analysis
- `figures <https://bitbucket.org/bbglab/nucleosome-periodicity/src/master/figures/>`_: code to generate the figures and tables for the paper
- `germline <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/nucleosome-periodicity/raw/master/germline/germline.ipynb>`_: germline data analysis
- `increase <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/nucleosome-periodicity/raw/master/increase/increase.ipynb>`_: code for the increase of mutation rate analysis
- `mutations <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/nucleosome-periodicity/raw/master/mutations/mutations.ipynb>`_:  mutational data analysis
- `nucleosomes <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/nucleosome-periodicity/raw/master/nucleosomes/nucleosomes.ipynb>`_:  computation of the dyads positions
- `periodicity <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/nucleosome-periodicity/raw/master/periodicity/periodicity.ipynb>`_:  WW periodicity analysis
- `rotational <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/nucleosome-periodicity/raw/master/rotational/rotational.ipynb>`_:  rotational classification of the nucleosomes
- `signatures <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/nucleosome-periodicity/raw/master/signatures/signatures.ipynb>`_:  analysis of the signatures of the mutational data
- `simulation <http://nbviewer.jupyter.org/urls/bitbucket.org/bbglab/nucleosome-periodicity/raw/master/simulation/simulation.ipynb>`_:  simulation

Each folder contains a notebook with a brief description and
the requirements (notebooks that need to be executed).


Running this software
---------------------

These analysis have been perform using software in Python, R and GNU bash.

We have created a set of `Jupyter notebooks <http://jupyter.org/>`_
that you can run if you are interested in re-running partially or
totally our analysis.
In each notebook you will find further details for running them.

Requirements
************

To be able to run those notebooks you need to have the following
software installed (we also indicate the version so you can
reproduce the exact same results):

   Python (3.5.6) Packages:

   - `ipykernel <https://pypi.org/project/ipykernel/>`_ (4.8.2)
   - `numpy <http://www.numpy.org/>`_ (1.15.1)
   - `pandas <https://pandas.pydata.org/>`_ (0.23.4)
   - `scipy <https://www.scipy.org/>`_ (1.1.0)
   - `matplotlib <https://matplotlib.org/>`_ (2.2.3)
   - `statsmodels <https://www.statsmodels.org/stable/index.html>`_ (0.9.0)
   - `click <http://click.pocoo.org>`_ (6.7)
   - `tqdm <https://pypi.org/project/tqdm>`_ (4.25.0)
   - `intervaltree <https://pypi.org/project/intervaltree>`_ (2.1.0)
   - `lmfit <https://lmfit.github.io/lmfit-py>`_ (0.9.11)
   - `rpy2 <https://rpy2.readthedocs.io/en/latest/>`_ (2.7.8)
   - `bgreference <https://bitbucket.org/bgframework/bgreference>`_ (0.5)
   - `xlrd <http://www.python-excel.org/>`_ (1.1.0)

   Python (2.7.15) Packages:

   - `crossmap <http://crossmap.sourceforge.net>`_ (0.2.7) [#envcrossmap]_

   R (3.4.3) packages:

   - `deconstructSigs <https://github.com/raerose01/deconstructSigs>`_ (1.8.0) [#envdeconstruct]_
   - `sigfit <https://github.com/kgori/sigfit>`_ (1.0.0)  [#envsigfit]_

   Other software:

   - `bedops <https://bedops.readthedocs.io/en/latest/>`_ (2.4.35)
   - `bedgraphtobigwig <http://hgdownload.soe.ucsc.edu/admin/exe/>`_ (366)
   - `bigwigtowig <http://hgdownload.soe.ucsc.edu/admin/exe/>`_ (366)
   - `bedtools <https://bedtools.readthedocs.io/en/latest/>`_ (v2.27.1)
   - `sra-tools <https://github.com/ncbi/sra-tools>`_ (2.9.1)
   - `bowtie <http://bowtie-bio.sourceforge.net/index.shtml>`_ (1.2.2)
   - `vcftools <https://vcftools.github.io/index.html>`_ (0.1.16)
   - `fa-split <http://hgdownload.soe.ucsc.edu/admin/exe/>`_ (366)
   - `bwtool <https://github.com/CRG-Barcelona/bwtool/wiki>`_ (1.0) [#noconda]_
   - `awk <http://www.cs.princeton.edu/~bwk/btl.mirror/>`_ (4.0.2) [#noconda]_
   - `sort <http://www.gnu.org/software/coreutils/>`_ (8.22) [#noconda]_
   - `gzip <https://www.gnu.org/software/gzip/>`_ (1.5) [#noconda]_
   - `grep <https://www.gnu.org/software/grep/manual/grep.html>`_ (2.20) [#noconda]_
   - `basename <http://www.gnu.org/software/coreutils/>`_ (8.22) [#noconda]_
   - `mktemp <http://www.gnu.org/software/coreutils/>`_ (8.22) [#noconda]_

In addition, we have created a Python package named ``nucperiod`` that contains a set of
python scripts that we have used during our analysis.
In can be installed with pip:

.. code:: python

   cd nucperiod
   pip install .

For some of the analyses (those where CrossMap, DeconstructSigs and SigFit are involved)
we already prepared three `conda environments <https://conda.io/docs/>`_:

- ``env_crossmap`` environment for Crossmap as it is a Python 2.7 tool
  (you can create it with ``env_crossmap.yml``)
- ``env_deconstructsigs`` environment for the deconstructSigs
  R package (use the ``env_deconstructsigs.yml`` to replicate it)
- ``env_nucperiod_sigfit`` environment for the SigFit R package.
  Please, note that the package is not installed in that environment
  and you need to install it manually


Notes
*****

Most of the intermediate files generated while running any notebook
are most likely not used for further analysis.
However, we have decided not to remove them so you can
check them if needed.

Compressing most of the files is not needed, however, we
have decided to do that in order to save disk space.

The scripts that you can find in the ``scripts`` directories
are documented for further info.
If you want to check which parameters
each script accepts, use the ``--help`` flag
(`python <script> --help`).


Fixing datasets versions
------------------------

This project makes use of datasets available thought the
`bgdata <https://bitbucket.org/bgframework/bgdata>`_.
This package will try to download the latest version,
however, you can fix the version of these datasets easily.
After installing the package, update the file
``~/.bbglab/bgdata.conf`` to add the following lines::

    [datasets/genomereference/hg19]
    build = 20150724
    [datasets/genomereference/tair10]
    build = 20180810
    [datasets/genomereference/saccer3]
    build = 20180720
    [datasets/genomereference/dm3]
    build = 20180904
    [datasets/genomereference/mm9]
    build = 20171103
    

Dyads positions
------------------------

The 147 bp length mid-fragments of high-coverage MNAse-seq reads (representing putative nucleosome dyads) mapped to the hg18 human genome assembly were obtained from (Gaffney et al., 2012) in wig format, and converted to the bed format using the wig2bed utility from BEDOPS (Neph et al., 2012). To find the most representative dyads, we first smoothed-out the mid-fragment counts using a 15-bp tri-weight kernel similar to that described in (Valouev et al., 2011). This method proceeds in two steps. First, we retrieved the raw dyad count per position, , and smoothed-out the resulting signal using the kernel

.. image:: https://latex.codecogs.com/gif.download?K%28x%29%20%3D%20%281-%28%5Cfrac%7Bx%7D%7B15%7D%29%5E%7B2%7D%29%5E%7B3%7D%5Ccdot1_%7B%7Cx%7C%3C15%7D

thereby producing the kernel-smoothed dyad count

.. image:: https://latex.codecogs.com/gif.download?D%28i%20%29%3D%5Csum_%7Bj%3D1%7D%5E%7BN%7D%20K%28i-j%29%20%5Ccdot%20d%28j%29

where  is the length of the sequence. Second, at each position , we correct  dividing by an approximation of the total number of counts in the -bp interval, resulting in the following stringency metric:

.. image:: https://latex.codecogs.com/gif.download?S%28i%20%29%3D%5Cfrac%7BD%28i%29%7D%7B%5Csum_%7Bj%3Di-150%7D%5E%7Bi+150%7D%20%5Calpha%20%5Ccdot%20%5Cfrac%7B1%7D%7B15%7D%20%5Ccdot%20D%28j%29%7D%2C%20with%20%5Cfrac%7B1%7D%7B%5Calpha%7D%3D%20%5Cint_%7B-1%7D%5E1%281-u%5E2%29%5E3du.

The resulting bed files with the smoothed dyads of each chromosome were merged and converted into a wig file using the BedgraphToBigWig tool (Kent et al., 2010). The local maxima of the smoothed counts, which represent the highest fraction of “well positioned” nucleosomes covering a position, were obtained using bwtool (Pohl and Beato, 2014) with the parameters “find local-extrema -maxima -min-sep=150.” The mid-fragment with the highest number of reads within a 30 bp interval was selected to contain the putative and most representative dyad, and any other mid-fragment in that interval was discarded. In case of a tie between two or more mid-fragments, the dyad closest to each local maxima was selected. Dyad coordinates were lifted over from hg18 to hg19 using CrossMap (Zhao et al., 2014). We extended the sequences around dyads by 73 bp at each side, and we kept the resulting nucleosome-covered sequences only if all their nucleotides were mappable according to the CRG36 Alignability track.

To obtain the set of intergenic nucleosomes, we retrieved the coordinates of genic regions from Gencode (`Harrow et al., 2012 <https://pubmed.ncbi.nlm.nih.gov/22955987/>`_), extended them by 500 bp on each side of their start and end boundaries, and removed all nucleosomes overlapping the extended regions. The final human dataset of intergenic nucleosomes comprised 3,759,105 instances. We call this subset the human nucleosome set: in mappable non-genic regions (`download <https://bitbucket.org/bbglab/nucleosome-periodicity/raw/master/nucleosomes/output/dyads.bed.gz>`_) and nucleosomes falling in genic regions (`download <https://bitbucket.org/bbglab/nucleosome-periodicity/raw/master/nucleosomes/output/dyads_genic.bed.gz>`_).

If you use these files, please make sure to cite `Gaffney et al., 2012 <https://journals.plos.org/plosgenetics/article?id=10.1371/journal.pgen.1003036>`_ in addition to `Pich et al., 2018 <https://www.cell.com/cell/fulltext/S0092-8674(18)31312-6?_returnURL=https%3A%2F%2Flinkinghub.elsevier.com%2Fretrieve%2Fpii%2FS0092867418313126%3Fshowall%3Dtrue>`_.


----


.. [#noconda] This software was *not* installed within
   a conda environment.

.. [#envcrossmap] This package has been installed in a separate environment
   named as ``env_crossmap``

.. [#envdeconstruct] This package has been installed in a separate environment
   named as ``env_deconstructsigs``

.. [#envsigfit] This package has been installed in a separate environment
   named as ``env_sigfit``

